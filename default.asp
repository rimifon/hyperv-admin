<!-- #include file="AspLib/api.asp" --><%
function boot(route) {
	sys.name = "Hyper-V 管理";
	sys.dir = {
		vpsRoot: "D:\\storages\\hyperv\\Free",
		vhdRoot: "D:\\storages\\disk"
	};
	// 支持的系统类型（前缀为 sys.dir.vhdRoot，需自行预装系统）
	sys.source = {
		alpine: "\\Source\\Alpine.vhdx",
		win2019: "\\Source\\Win2019.vhdx",
		debian: "\\Source\\Debian.vhdx",
		deepin: "\\Source\\Deepin.vhdx",
		win11: "\\Source\\Win11.vhdx"
	};
	return apidoc({
		Memo: [
			"* 目前支持的操作系统类型有：Alpine, Win2019, Debian, Deepin, Win11",
			"* 从 方法 7 开始是散装零件，仅作参考，不建议调试。" + "【VPS 列表页】".link("?r=ListPage") + " | " + "【Node入职】".link("BBS/?r=topic/1"),
			"ResourceType: { 3: CPU, 4: 内存, 5: IDE 插槽, 10: 网络适配器 17: 硬盘驱动器, 31: 硬盘镜像, 33: 网络设置 }"
		],

		// 列表页
		listpage: function() {
			var vpsList = this.vpslist();
			var network = wmi().InstancesOf("Msvm_GuestNetworkAdapterConfiguration");
			var ips = new Object;
			for(var i = 0; i < network.Count; i++) {
				var adp = network.ItemIndex(i);
				var vpsid = adp.InstanceID.split("\\")[1];
				ips[vpsid] = adp.IPAddresses.toArray().join(", ");
			}
			%><!-- #include file="views/listpage.html" --><%
		},

		NewVpsDoc: [ "新建虚拟机", "vpsName, system, cpu, mem", "内存 mem 单位为 GB" ],
		newvps: function() {
			var vpsName = form("vpsName") || "NewVPS";
			var system = (form("system") || "alpine").toLowerCase();
			var cpu = ~~form("cpu") || 1;
			var mem = ~~form("mem") || 1;
			if(/^[^\w \-\.]$/.test(vpsName)) return { err: "虚拟机名称不能包含特殊字符" };
			if(!sys.source[system]) return { err: "不支持的系统类型" };
			// 判断此名称的虚拟机是否创建过了
			var vps = wmi().ExecQuery("select * from CIM_System where ElementName='" + vpsName + "'");
			if(vps.Count) return { err: "虚拟机名称【" + vpsName + "】已被使用" };
			// 创建硬盘是异步操作，先创建硬盘镜像
			var rs = this.vhdcreate(vpsName, system);
			if(rs.err) return { err: res.err, step: "创建硬盘" };
			// 创建虚拟机
			rs = this.vpscreate(vpsName, system);
			if(rs.err) return { err: rs.err, step: "创建虚拟机" };
			// 挂载网络适配器
			var vpsid = wmi().Get(rs.ResultingSystem).Name;
			rs = this.vpsattacheth(vpsid, "Default Switch");
			if(rs.err) return { err: rs.err, step: "挂载网络适配器" };
			if(system == "win11") this.scsiattach(vpsid);
			// 挂载硬盘到虚拟机
			rs = this.vpsattachvhd(vpsid, sys.dir.vhdRoot + "\\Free\\" + vpsName + ".vhdx", system == "win11" ? 6 : 5);
			if(rs.err) return { err: rs.err, step: "挂载硬盘" };
			// 开机
			rs = this.startup(vpsid);
			if(rs.err) return { err: rs.err, step: "开机" };
			return { msg: "虚拟机【" + vpsName + "】创建成功，默认密码为：fundot" };
		},

		DropVpsDoc: [ "删除虚拟机", "vpsName", "此 vpsName 为虚拟机友好名称" ],
		dropvps: function() {
			var vpsName = form("vpsName");
			if(/^[^\w \-\.]$/.test(vpsName)) return { err: "虚拟机名称不能包含特殊字符" };
			var vps = wmi().ExecQuery("select * from CIM_ComputerSystem where ElementName='" + vpsName + "'");
			if(vps.Count < 1) return { err: "虚拟机【" + vpsName + "】不存在" };
			vps = vps.ItemIndex(0);
			var vss = wmi().Get("Msvm_VirtualSystemSettingData.InstanceID='Microsoft:" + vps.Name + "'");
			if(vss.Version != "9.0") return { err: "此虚拟机暂时只能由管理员删除。" };
			if(vps.EnabledState != 3) {
				vps.RequestStateChange(4);
				return { err: "已尝试关闭虚拟机【" + vps.Name + "】，请稍后重试。" };
			}
			var vsms = wmi().ExecQuery("select * from Msvm_VirtualSystemManagementService").ItemIndex(0);
			// 先删除虚拟机会导致虚拟硬盘文件被占用，所以先删虚拟硬盘。
			var fso = new ActiveXObject("Scripting.FileSystemObject");
			fso.DeleteFile(sys.dir.vhdRoot + "\\Free\\" + vpsName + ".vhdx");
			vsms.DestroySystem(vps.Path_.Path);
			return { msg: "虚拟机【" + vpsName + "】删除完成" };
		},

		ServiceStateDoc: [ "服务运行状态" ],
		servicestate: function() {
			var rs = wmi().InstancesOf("Msvm_VirtualSystemManagementService");
			if(!rs.Count) return { err: "服务未安装" };
			var arr = rsToArr(rs, "Caption, Description, InstallDate, StatusDescriptions")[0];
			arr.InstallDate = parseTime(arr.InstallDate);
			return arr;
		},

		VpsListDoc: [ "VPS 列表" ],
		vpslist: function() {
			var rs = wmi().ExecQuery("select * from Cim_System where Caption='虚拟机'");
			var arr = rsToArr(rs, "ElementName, EnabledState, Name, InstallDate, OnTimeInMilliseconds");
			var dic = [ "", "", "运行中", "已关机" ]; dic[10] = "启动中";
			for(var i = 0; i < arr.length; i++) {
				arr[i].EnabledState = dic[arr[i].EnabledState];
				arr[i].InstallDate = parseTime(arr[i].InstallDate);
				if(arr[i].OnTimeInMilliseconds == 0) continue;
				arr[i].BootTime = new Date(sys.sTime - arr[i].OnTimeInMilliseconds).getVarDate();
			}
			return arr;
		},

		JobQueryDoc: [ "异步操作任务结果查询", "jobPath", "某些操作是异步的，需要客户端延时发 Ajax 查询结果，然后才可执行下一步操作。" ],
		jobquery: function() {
			var jobPath = form("jobPath");
			if(!jobPath) return { err: "jobPath 不能为空" };
			try { var job = wmi().Get(jobPath); }
			catch(err) { return { err: err.message }; }
			return { Description: job.Description, OperationalStatus: job.OperationalStatus.toArray(), StatusDescriptions: job.StatusDescriptions.toArray() };
		},

		VpsCreateDoc: [ "创建 VPS", "name, system" ],
		vpscreate: function(name, system, mem, cpu) {
			if(!name) name = form("name") || "NewVPS";
			var vsms = wmi().InstancesOf("Msvm_VirtualSystemManagementService");
			if(!vsms.Count) return { err: "服务未安装" };
			var rs = wmi().Get("Msvm_VirtualSystemSettingData").SpawnInstance_();
			rs.ElementName = name;
			rs.Version = "9.0";
			rs.VirtualSystemSubType = "Microsoft:Hyper-V:SubType:" + (system == "win11" ? 2 : 1);
			rs.ConfigurationDataRoot = sys.dir.vpsRoot;
			var inParam = vsms.ItemIndex(0).Methods_("DefineSystem").InParameters.SpawnInstance_();
			inParam.SystemSettings = rs.GetText_(2);
			var outParam = vsms.ItemIndex(0).ExecMethod_("DefineSystem", inParam);
			var code = outParam.ReturnValue;
			if(code != 0) return { code: code, err: "操作异常", Job: outParam.Job };
			return { msg: "虚拟机创建成功", ResultingSystem: outParam.ResultingSystem };
		},

		SCSIAttachDoc: [ "添加 SCSI 控制器", "vpsName", "vpsName 为虚拟机 GUID", "* 注：二代虚拟机默认没有磁盘控制器，需调用此方法添加。" ],
		scsiattach: function(vpsName) {
			if(!vpsName) vpsName = form("vpsName");
			var vsms = wmi().InstancesOf("Msvm_VirtualSystemManagementService").ItemIndex(0);
			var vps = wmi().ExecQuery("select * from CIM_VirtualSystemSettingData where InstanceID='Microsoft:" + vpsName + "'");
			var rs = wmi().ExecQuery("select * from CIM_ResourceAllocationSettingData where ResourceType=6 and InstanceID like '%Default'");
			var code = vsms.AddResourceSettings(vps.ItemIndex(0).Path_.Path, [ rs.ItemIndex(0).GetText_(2) ]);
			return { code: code, msg: !code ? "SCSI 添加成功" : "不知道附加结果" };
		},

		VhdCreateDoc: [ "创建虚拟硬盘", "vhdName, source", "vhdName: 不需要.vhdx 后缀", "source: 定义的系统类型，例如：Alpine" ],
		vhdcreate: function(vhdName, source) {
			if(!vhdName) vhdName = form("vhdName") || "NewVHD";
			if(!source) source = form("source") || "alpine";
			source = sys.source[source.toLowerCase()];
			if(!source) return { err: "没有找到指定的系统类型" };
			var ims = wmi().InstancesOf("Msvm_ImageManagementService").ItemIndex(0);
			var newVhd = wmi().Get("Msvm_VirtualHardDiskSettingData").SpawnInstance_();
			newVhd.Format = 3;
			newVhd.Type = 4;
			newVhd.Path = sys.dir.vhdRoot + "\\Free\\" + vhdName + ".vhdx";
			newVhd.ParentPath = sys.dir.vhdRoot + source;
			var inParam = ims.Methods_("CreateVirtualHardDisk").InParameters.SpawnInstance_();
			inParam.VirtualDiskSettingData = newVhd.GetText_(2);
			var outParam = ims.ExecMethod_("CreateVirtualHardDisk", inParam);
			var code = outParam.ReturnValue;
			var dic = { 32773: "参数无效" };
			if(code != 4096) return { code: code, err: dic[code] };
			var job = wmi().Get(outParam.Job);
			return { Description: job.Description, Status: job.StatusDescriptions.toArray(),  Job: outParam.Job };
		},

		VpsAttachVhdDoc: [ "挂载虚拟硬盘", "vpsName, vhdPath, ctlType", "vpsName 为 虚拟机 GUID", "ctlType: { 5: IDE, 6: SCSI }" ],
		vpsattachvhd: function(vpsName, vhdPath, ctlType) {
			if(!vpsName) vpsName = form("vpsName");
			if(!vhdPath) vhdPath = form("vhdPath");
			if(!ctlType) ctlType = form("ctlType") || 5;
			// 查找该虚拟机的 IDE 0 号插槽
			var ide = wmi().ExecQuery("select * from CIM_ResourceAllocationSettingData where ResourceType=" + ctlType + " and InstanceID like 'Microsoft:" + vpsName + "%'");
			if(!ide.Count) return { err: "没有可用的 IDE 或 SCSI 插槽" };
			// 定义新的硬盘驱动器
			var drv = wmi().Get("Msvm_ResourceAllocationSettingData").SpawnInstance_();
			drv.AddressOnParent = "0";	// 放入 IDE0 的 0 号插槽
			drv.ResourceType = 17;		// 硬盘驱动器
			drv.ResourceSubType = "Microsoft:Hyper-V:Synthetic Disk Drive";
			drv.Parent = ide.ItemIndex(0).Path_.Path;	// IDE0
			// 附加硬盘驱动器到VPS
			var vsms = wmi().InstancesOf("Msvm_VirtualSystemManagementService").ItemIndex(0);
			var inParam = vsms.Methods_("AddResourceSettings").InParameters.SpawnInstance_();
			var vps = wmi().ExecQuery("select * from CIM_VirtualSystemSettingData where InstanceID='Microsoft:" + vpsName + "'");
			inParam.AffectedConfiguration = vps.ItemIndex(0).Path_.Path;
			inParam.ResourceSettings = [ drv.GetText_(2) ];
			var outParam = vsms.ExecMethod_("AddResourceSettings", inParam);
			var code = outParam.ReturnValue;
			if(code != 0) return { err: "硬盘驱动器参数可能有错", code: code, Job: outParam.Job };
			// 定义新的硬盘映像
			var vhd = wmi().Get("Msvm_ResourceAllocationSettingData").SpawnInstance_();
			vhd.ResourceType = 31;
			vhd.ResourceSubType = "Microsoft:Hyper-V:Virtual Hard Disk";
			vhd.HostResource = [ vhdPath ];
			vhd.Parent = outParam.ResultingResourceSettings.getItem(0);
			var code = vsms.AddResourceSettings(vps.ItemIndex(0).Path_.Path, [ vhd.GetText_(2) ]);
			return { code: code, msg: !code ? "硬盘附加成功" : "硬盘镜像参数可能有错" };
		},

		VpsAttachEthDoc: [ "挂载网卡", "vpsName, switchName", "vpsName 为 虚拟机 GUID", "switchName 默认为“Default Switch”" ],
		vpsattacheth: function(vpsName, switchName) {
			if(!vpsName) vpsName = form("vpsName");
			if(!switchName) switchName = form("switchName") || "Default Switch";
			var vps = wmi().ExecQuery("select * from CIM_ComputerSystem where Name='" + vpsName + "'");
			if(!vps.Count) return { err: "没有找到虚拟机" };
			var sw = wmi().ExecQuery("select * from Msvm_VirtualEthernetSwitch where ElementName='" + switchName + "'");
			if(!sw.Count) return { err: "没有找到交换机" };
			var vsms = wmi().InstancesOf("Msvm_VirtualSystemManagementService").ItemIndex(0);	// 用于操作虚拟机的工具
			// 装网卡（使用默认的网卡参数）
			var sep = wmi().Get("Msvm_SyntheticEthernetPortSettingData.InstanceID='Microsoft:Definition\\6A45335D-4C3A-44B7-B61F-C9808BBDF8ED\\Default'");
			var inParam = vsms.Methods_("AddResourceSettings").InParameters.SpawnInstance_();
			inParam.AffectedConfiguration = vps.ItemIndex(0).Path_.Path;
			inParam.ResourceSettings = [ sep.GetText_(2) ];
			var outParam = vsms.ExecMethod_("AddResourceSettings", inParam);
			var code = outParam.ReturnValue;
			if(code != 0) return { err: "网卡参数可能有错", code: code, Job: outParam.Job };
			// 插网线（使用默认的连接参数）
			var etc = wmi().Get("Msvm_EthernetPortAllocationSettingData.InstanceID='Microsoft:Definition\\72027ECE-E44A-446E-AF2B-8D8C4B8A2279\\Default'");
			etc.Parent = outParam.ResultingResourceSettings.getItem(0);	// 指定网卡
			etc.HostResource = [ sw.ItemIndex(0).Path_.Path ];	// 指定交换机
			var code = vsms.AddResourceSettings(vps.ItemIndex(0).Path_.Path, [ etc.GetText_(2) ]);
			if(code != 0) return { err: "连接参数可能有错", code: code };
			return { msg: "网络连接成功", Path: etc.Parent };
		},

		StartUpDoc: [ "虚拟机开机", "vmName", "vmName 为虚拟机 GUID" ],
		startup: function(vmName) {
			if(!vmName) vmName = form("vmName");
			var vps = wmi().execQuery("select * from CIM_System where Name='" + vmName + "'");
			if(!vps.Count) return { err: "没有找到虚拟机" };
			var state = vps.ItemIndex(0).EnabledState;
			if(state != 3) return { code: 0, err: "当前状态[" + state + "]暂时不支持开机" };
			var code = vps.ItemIndex(0).RequestStateChange(2);
			if(code != 4096) return { err: "操作失败", code: code };
			return { code: code, msg: "命令发送成功" };
		},

		ShutdownDoc: [ "虚拟机关机", "vmName, force", "vmName 为虚拟机 GUID", "force=1时 强制关机" ],
		shutdown: function(vmName, force) {
			if(!vmName) vmName = form("vmName");
			if(!force) force = form("force");
			var vps = wmi().execQuery("select * from CIM_System where Name='" + vmName + "'");
			if(!vps.Count) return { err: "没有找到虚拟机" };
			var state = vps.ItemIndex(0).EnabledState;
			if(state != 2) return { code: 0, err: "当前状态[" + state + "]暂时不支持关机" };
			var code = vps.ItemIndex(0).RequestStateChange(force == 1 ? 3 : 4);
			if(code != 4096) return { err: "操作失败", code: code };
			return { code: code, msg: "命令发送成功" };
		}
	}, route);
}

function wmi() { return !wmi.ins ? wmi.ins = GetObject("winmgmts://localhost/root/virtualization/v2") : wmi.ins; }

function rsToArr(rs, fields) {
	var arr = new Array, cols = fields.split(/[\s\,]+/);
	var enm = new Enumerator(rs);
	while (!enm.atEnd()) {
		var row = enm.item(), obj = new Object;
		for (var i = 0; i < cols.length; i++) {
			var x = row[cols[i]];
			if("unknown" == typeof x) x = x.toArray();
			obj[cols[i]] = x;
		}
		arr.push(obj); enm.moveNext();
	}
	sys.wmiRs = rs;
	return arr;
}

function parseTime(str) { return (str + "").replace(/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2}).*$/, "$1-$2-$3 $4:$5:$6"); }
%>